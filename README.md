# disgenetplus2r

`disgenetplus2r`: An R package to explore DISGENET plus

**<span style="color:red; font-size:18px;">IMPORTANT: The disgenetplus2r package is no longer updated and will soon be replaced by disgenet2r.</span>**  

A detailed documentation of the disgenet2r is available [here](#https://medbio.gitlab.io/disgenet2r/).

## Package' Status

 * __Version__: 1.0.7
 * __Subversion__: `20240502`
 * __Authors__:  MEDBIOINFORMATICS SOLUTIONS SL
 * __Maintainer__: <janet.pinero@disgenet.com>

## How to start

### Installation

The package, `disgenetplus2r` can be installed using `devtools` from this repository:

```R
library(devtools)
install_gitlab("medbio/disgenetplus2r")
```

### Obtaining the API key

Before using the package, you will need to register for a DISGENET plus account at [https://www.disgenetplus.com/](https://www.disgenetplus.com/). Once you have completed the registration process, retrieve your API key in your user profile.
After retrieving the API key, run the line below so the key is available for all the **disgenetplus2r** functions. 

```{r, warning = FALSE, message = FALSE, eval=T}
api_key <- "enter your API key here"
Sys.setenv(DISGENETPLUS_API_KEY= api_key)

```


### Querying DISGENET plus data:

The following lines show two examples of how DISGENET plus can be queried using `disgenetplus2r`:

 * __Gene Query__

```R
res <- gene2disease(gene = 3953, 
 vocabulary = "ENTREZ",
    database = "ALL", 
    score = c( 0.1,1)
)

```

 * __Disease Query__

```R
res <- disease2gene(disease = "UMLS_C0028754", 
    database = "ALL",
    score = c(0.3,1) 
)
```


 * __Variant Query__

```R
res <-  variant2disease(variant = "rs1800629",
       database = "ALL",
         score =c(0,1)
)
```

A detailed documentation of the package is available [here](https://medbio.gitlab.io/disgenetplus2r/).



## COPYRIGHT

©2024 MedBioinformatics Solutions SL


## License

disgenetplus2r is distributed under the GPL-2 license. 


 

