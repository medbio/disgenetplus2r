# disgenetplus2r 1.0.7

## Bug fixes

an url encoder function was added to the **get_disgenetplus_api_key** to avoid problems with passwords that contain "&".

# disgenetplus2r 1.0.6

## New Features

-   Functions **gene2chemical**,**disease2chemical** ,**variant2chemical** ,and **chemical2evidence**, now return a new field, `chemical_effect`, with possible values: toxicity, therapeutic and unrelated

# disgenetplus2r 1.0.5

## Bug fixes

- Fixed functions chemical2disease, chemical2gene, and chemical2variant, that produced repeated results. 

# disgenetplus2r 1.0.4

## New Features

- DiseaseClass plots are possible for objects of type chemical-disease
- ProteinClass plots are possible for objectos of type chemical-gene
- Protein Class and Disease Class plots for single entity searches show node sizes proportional to the number of proteins/diseases in class, for both, interactive and non-interactive plots
- added new parameter (eprop) to network plots that allows to manipulate edge width

## Refactoring

-   plot error messages have all the same format

## Bug fixes

- fixed protein class plots that showed empty values as nodes


# disgenetplus2r 1.0.3

## New Features

-   Queries with functions **disease2variant**, **chemical2variant**, and **chemical2vda** and now can receive SIFT and POLYPHEN ranges

## Documentation

-   NEWS.md file was added to document package changes.
-   all functions that use the parameter `database` now have all datasources, including RGD and MGD, that are new sources

## Bug fixes

-   Functions that use the parameter `database` that query variant endpoints now throw an error if a database is not GWASCAT, CLINVAR, UNIPROT, TEXTMINING_HUMAN, CURATED, or ALL.
