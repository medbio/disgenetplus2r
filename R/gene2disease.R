#' Retrieves diseases associated to a gene, or list of genes and generates an \code{DataGeNET.DGN}
#'
#' Given the NCBI identifier, or HGNC symbol of one or multiple genes, retrieves their associated diseases
#' from DISGENET plus and creates an object of type \code{DataGeNET.DGN}.
#'
#' @param gene a gene, or a list of genes identified by NCBI identifiers, ENSEMBL identifiers, or HGNC symbols.
#' The genes non contained in DISGENET plus will be removed from the output.
#' @param vocabulary   The vocabulary of the gene identifier(s)
#' Select one of the available:  \code{HGNC} (Gene Symbols), \code{ENTREZ} (NCBI Gene Identifiers), \code{ENSEMBL} (ENSEMBL Gene Identifiers)
#' @param database Name of the database that will be queried. It can take the values:
#' \code{'CLINGEN'} to use Clinical Genome Resource;
#' \code{'CLINVAR'} to use ClinVar;
#' \code{'ORPHANET'}, to use Orphanet, the portal for rare diseases and orphan drugs;
#' \code{'PSYGENET'} to use PSYGENET;
#' \code{'UNIPROT'} to use Universal Protein Resource;
#' \code{'CURATED'} to use expert curated, human databases;
#' \code{'HPO'} to use HPO;
#' \code{'GWASCAT'} to use the NHGRI-EBI GWAS Catalog;
#' \code{'PHEWASCAT'} to use the NHGRI-EBI GWAS Catalog;
#' \code{'INFERRED'} to use inferred data from HPO,and GWASCAT;
#' \code{'MGD_HUMAN'} to use MGD, human data;
#' \code{'MGD_MOUSE'} to use MGD, mouse data;
#' \code{'RGD_HUMAN'} to use RGD, human data;
#' \code{'RGD_RAT'} to use RGD, rat data;
#' \code{'TEXTMINING_MODELS'} to use text mining animal models data generated using own system;
#' \code{'MODELS'} to use the expert curated, animal models data;
#' \code{'TEXTMINING_HUMAN'} to use text mining human data, generated using own system;
#' \code{'ALL'} to use all these databases. Default \code{'CURATED'}.
#' @param score A vector with two elements: 1) initial value of score 2) final value of score
#' @param verbose By default \code{FALSE}. Change it to \code{TRUE} to get a
#' on-time log from the function.
#' @param warnings By default \code{TRUE}. Set to \code{FALSE} to hide the warnings.
#' @param n_pags A number between 0 and 100 indicating the number of pages to retrieve from the results of the query. Default \code{100}
#' @param api_key An api key to connect to DISGENET plus. See more at https://api.disgenetplus.com/
#' @return An object of class \code{DataGeNET.DGN}
#' @importFrom dplyr "%>%"
#' @examples
#' g <- gene2disease( gene =3953, "CURATED", score=c(0,1), vocabulary = "ENTREZ" )
#' @export gene2disease


gene2disease <- function( gene, vocabulary= "HGNC", n_pags = 100,api_key=NULL, chemical = NA, database = "CURATED", score=c(0,1), verbose = FALSE, warnings = TRUE ) {
  if(is.null(api_key)){
    api_key = Sys.getenv('DISGENETPLUS_API_KEY')
    if(api_key == ""){
      stop("This is not a valid API KEY! Please, use the function `get_disgenetplus_api_key` to get your API key from DISGENET plus")
    }
  }
  check_disgenet_sources( database )

  if( length( gene ) != length( unique( gene ) ) ) {
    gene <- unique( gene )
    warning(
      "Removing duplicates from input genes list."
    )
  }
  list_of_genes <- paste(gene,collapse=",")
  type <- "symbol"
  if( vocabulary == "HGNC" ){
    url <- paste0( get_url_disgenetplus(), "gda/summary?gene_symbol=", list_of_genes , "&source=",database, "&min_score=",score[1],"&max_score=", score[2])
  } else if( vocabulary == "ENTREZ" ){
    type <- "entrez"
    url <- paste0( get_url_disgenetplus(), "gda/summary?gene_ncbi_id=", list_of_genes , "&source=",database, "&min_score=",score[1],"&max_score=", score[2])
    #print(url)
  } else if( vocabulary == "ENSEMBL" ){
    type <- "ensembl"
    url <- paste0( get_url_disgenetplus(), "gda/summary?gene_ensembl_id=", list_of_genes , "&source=",database, "&min_score=",score[1],"&max_score=", score[2])
  }   else if( class( gene[1] ) == "factor"){
    message("Your input genes are in factor format.
    Please, revise your input genes and save them as numeric or as character.")
    stop()
  } else {
    message(paste0("Your input genes are a wrong vocabulary ", vocabulary,
                   "Please, revise your input vocabulary. Remember that genes should be identified
                   using HGNC gene symbols or NCBI Entrez identifiers"))
    stop()
  }

  if( !is.na(chemical)){
    url <- paste0(url, "&chemical=",chemical)
  }

  if(length(score) != 2) {
    stop("Invalid argument 'score'. It must have two elements: initial value of the score, and final value of the score")
  }
  result  <- get_data_api(url = url, n_pags = n_pags)
  if(dim(result)[1]>0){
    colnames(result) <-gsub("geneSymbol_keywrod", "gene_symbol", colnames(result))
    colnames(result) <-gsub("symbolOfGene", "gene_symbol", colnames(result))

    colnames(result) <-gsub("geneNcbiID", "geneid", colnames(result))
    colnames(result) <-gsub("diseaseName", "disease_name", colnames(result))
    colnames(result) <-gsub("geneProteinStrIDs", "uniprotids", colnames(result))
    colnames(result) <-gsub("geneProteinClassNames", "protein_class_name", colnames(result))
    colnames(result) <-gsub("geneEnsemblIDs", "ensemblid", colnames(result))
    colnames(result) <-gsub("geneProteinClassIDs", "protein_classid", colnames(result))

    colnames(result)[which(colnames(result) == "el")] <- "evidence_level"
    colnames(result)[which(colnames(result) == "ei")] <- "evidence_index"
    ttype <- "gene-disease"

    if (!is.na(chemical)){
      result <- tidyr::unnest(result,  chemicalsIncludedInEvidence) #%>% filter(source == database)
      result <- result[ result$chemUMLSCUI %in% chemical,]
      # result <- result %>% select(-source)
      # result <- tidyr::unnest(result,  chemicalsIncludedInEvidence) %>% filter(source == database)
      result <- result %>% dplyr::select( -numberPmidsWithChemsIncludedInEvidenceBySource) %>% dplyr::rename(chemical_name = chemShowName, chemicalid = chemUMLSCUI)
      result <- result[ result$chemicalid %in% chemical,]
      ttype <- "chemical-gda"
    }

    if(length( gene )==1){
      cls <- "single"
    }else{
      cls <- "list"
    }

    scR <- paste0( score[1],"-", score[2])

    if(type == "symbol"){
      wGenes <- setdiff(gene, result$gene_symbol)
      eGenes <- intersect(gene, result$gene_symbol)
    }else if (type == "entrez"){
      wGenes <- setdiff(gene,result$geneid)
      eGenes <- intersect(gene, result$geneid)
    }
    else if (type == "ensembl"){
      wGenes <- setdiff(gene,result$ensemblid)
      eGenes <- intersect(gene, result$ensemblid)
    }
    result$diseaseid <-gsub("^.*UMLS_(C\\d{7})", "\\1", result$diseaseVocabularies)
    result$diseaseid <-gsub("\\)", "", result$diseaseid)
    result$diseaseid <-gsub('\\"', "", result$diseaseid)
    result <- result %>% dplyr::select(-"assocID", -"diseaseVocabularies")
    if( length( wGenes ) != 0 ) {
      genes <- paste( paste( "   -", wGenes ), collapse = "\n" )
      if( warnings ) {
        warning( "\n One or more of the genes in the list is not in DISGENET plus ( '", database, "' ):\n", genes )
      }
    }

    result <- methods::new( "DataGeNET.DGN",
                            type = "gene-disease",
                            search = cls,
                            term = as.character( eGenes ),
                            scoreRange = scR,
                            database = database,
                            qresult = result )

    return( result )
  } else {
    print("no results for the query")
  }
}
