#' Retrieves disease-disease associations via shared variants and generates an \code{DataGeNET.DGN}
#'
#' Given one or multiple diseases, the diseases that share variants with the query disease(s)
#' from DISGENET plus and creates an object of type \code{DataGeNET.DGN}.
#' @param disease a disease, or list of diseases using the following identifiers:
#' UMLS, ICD9CM, ICD10, MESH, OMIM, DO, EFO, NCI, HPO, MONDO, or ORDO.
#' The diseases non existing in DISGENET plus will
#' be removed from the output.
#' @param relationship the type of relationship of the disease pair. It can take the values:
#' \code{has_shared_genes}
#' \code{has_shared_variants}
#' \code{has_manifestation}
#' \code{has_associated_morphology}
#' \code{manifestation_of}
#' \code{associated_morphology_of}
#' \code{is_finding_of_disease}
#' \code{due_to}
#' \code{has_definitional_manifestation}
#' \code{has_associated_finding}
#' \code{definitional_manifestation_of}
#' \code{disease_has_finding}
#' \code{cause_of}
#' \code{associated_finding_of}
#' \code{is_similar_to}. Default \code{'has_shared_genes'}.
#' @param database Name of the database that will be queried. It can take the values:
#' \code{'CLINGEN'} to use Clinical Genome Resource;
#' \code{'CLINVAR'} to use ClinVar;
#' \code{'ORPHANET'}, to use Orphanet, the portal for rare diseases and orphan drugs;
#' \code{'PSYGENET'} to use PSYGENET;
#' \code{'UNIPROT'} to use Universal Protein Resource;
#' \code{'CURATED'} to use expert curated, human databases;
#' \code{'HPO'} to use HPO;
#' \code{'GWASCAT'} to use the NHGRI-EBI GWAS Catalog;
#' \code{'PHEWASCAT'} to use the NHGRI-EBI GWAS Catalog;
#' \code{'INFERRED'} to use inferred data from HPO,and GWASCAT;
#' \code{'MGD_HUMAN'} to use MGD, human data;
#' \code{'MGD_MOUSE'} to use MGD, mouse data;
#' \code{'RGD_HUMAN'} to use RGD, human data;
#' \code{'RGD_RAT'} to use RGD, rat data;
#' \code{'TEXTMINING_MODELS'} to use text mining animal models data generated using own system;
#' \code{'MODELS'} to use the expert curated, animal models data;
#' \code{'TEXTMINING_HUMAN'} to use text mining human data, generated using own system;
#' \code{'ALL'} to use all these databases. Default \code{'CURATED'}.
#' @param jg Minimum Jaccard coefficient computed from the genes associated to the diseases. Default \code{0}
#' @param jv Minimum Jaccard coefficient computed from the variants associated to the diseases. Default \code{0}
#' @param min_genes minimum number of shared genes between diseases. Default \code{0}
#' @param min_vars minimum number of shared variants between diseases. Default \code{0}
#' @param order_by A parameter to order the disease pairs.
#' Select one of  \code{SHARED_GENES} (shared genes), \code{SHARED_VARIANTS} (Shared variants),
#'  \code{JACCARD_GENES} (Jaccard coefficient computed from genes), \code{JACCARD_VARIANTS} (Jaccard coefficient computed from variants),
#'  \code{SOKAL} (Sokal-Sneath semantic similarity distance). Default \code{'SHARED_GENES'}.
#' @param verbose By default \code{FALSE}. Set to \code{TRUE} to get a
#' on-time log from the function.
#' @param warnings By default \code{TRUE}. Set to \code{FALSE} to hide the warnings.
#' @param n_pags A number between 0 and 100 indicating the number of pages to retrieve from the results of the query. Default \code{100}
#' @param api_key An api key to connect to DISGENET plus. See more at https://api.disgenetplus.com/
#' @return An object of class \code{DataGeNET.DGN}
#' @examples
#' dd1 <- disease2disease( "UMLS_C0028754", relationship = "has_manifestation"  )
#' @export disease2disease


disease2disease <-  function( disease, relationship = "has_shared_genes",  min_genes =0, min_vars= 0,
                              jg = 0, jv= 0, order_by="SHARED_GENES",
                              database = "CURATED", api_key=NULL,n_pags = 100,
                              verbose = FALSE, warnings = TRUE  ) {
  if(is.null(api_key)){
    api_key = Sys.getenv('DISGENETPLUS_API_KEY')
    if(api_key == ""){
      stop("This is not a valid API KEY! Please, use the function `get_disgenetplus_api_key` to get your API key from DISGENET plus")
    }
  }
  check_disgenet_sources( database )
  if ( relationship == "has_shared_variants" & ! database  %in% c("GWASCAT", "CLINVAR", "PHEWASCAT", "UNIPROT", "TEXTMINING_HUMAN",  "TEXTMINING","ALL", "CURATED") ){
    stop("This resource does not have variant-disease associations.")
  }
  # else if (! relationship %in% c("has_shared_variants", "has_shared_genes" & database != "ALL") ){
  #   if( warnings ) {
  #     warning(
  #       "The parameter database is not taken into account for this query."
  #     )
  #   }
  # }

  allowed_order_by = c("SHARED_GENES", "SHARED_VARIANTS",
                       "JACCARD_GENES",  "JACCARD_VARIANTS",
                       "SOKAL")
  if (!order_by %in% allowed_order_by) {
    stop(
      paste0(  order_by, " is an invalid parameter! Allowed parameters are: \n",
               paste(allowed_order_by, collapse = "\n")  )
    )
  }
  if( length( disease ) != length( unique( disease ) ) ) {
    disease <- unique( disease )
    if( warnings ) {
      warning(
        "Removing duplicates from input diseases list."
      )
    }
  }



  if(length( disease )==1){
    cls <- "single"
  }else {
    cls <- "list"
  }

  list_of_diseases <- paste(disease,collapse=",")

  type = "disease-disease-gene"
  if(relationship == "has_shared_genes"){
    url <- paste0( get_url_disgenetplus(), "dda?disease=", list_of_diseases , "&source=",database,
                   "&min_num_shared_genes=", min_genes, "&min_num_shared_variants=",min_vars,
                   "&min_jaccard_genes=",jg,
                   "&min_jaccard_variants=",jv, "&order_by=", order_by)
  }
  else if(relationship == "has_shared_variants"){
    url <- paste0( get_url_disgenetplus(), "dda?disease=", list_of_diseases , "&source=",database,
                   "&min_num_shared_genes=", min_genes, "&min_num_shared_variants=",min_vars,
                   "&min_jaccard_genes=",jg,
                   "&min_jaccard_variants=",jv, "&order_by=", order_by)
    type = "disease-disease-variant"
  }
  else if(relationship == "is_similar_to"){
    url <- paste0( get_url_disgenetplus(), "dda?disease=", list_of_diseases ,
                   "&min_sokal=",min_sokal, "&order_by=SOKAL")
    type     = "disease-disease-sokal"
  }
  else{
    # https://api.disgenetplus.com/api/v1/dda?disease=UMLS_C0028754&dda_relation=has_manifestation
    url <- paste0( get_url_disgenetplus(), "dda?disease=", list_of_diseases , "&dda_relation=",relationship, "&source=",database,
                   "&min_num_shared_genes=", min_genes, "&min_num_shared_variants=",min_vars,
                   "&min_jaccard_genes=",jg,
                   "&min_jaccard_variants=",jv, "&order_by=", order_by)
    type = "disease-disease-rela"
  }
  # https://api.disgenetplus.com/api/v1/dda?disease=UMLS_C0028754&source=CURATED&min_num_shared_genes=1&min_num_shared_variants=2&dda_relation=has_manifestation&order_by=JACCARD_GENES

  result  <- get_data_api(url = url, n_pags = n_pags)

  if ( dim(result)[1]> 0){
    result$diseaseid1 <-gsub("^.*UMLS_(C\\d{7})", "\\1", result$disease1_Vocabularies)
    result$diseaseid1 <-gsub("\\)", "", result$diseaseid1)
    result$diseaseid1 <-gsub('\\"', "", result$diseaseid1)

    result$diseaseid2 <-gsub("^.*UMLS_(C\\d{7})", "\\1", result$disease2_Vocabularies)
    result$diseaseid2 <-gsub("\\)", "", result$diseaseid2)
    result$diseaseid2 <-gsub('\\"', "", result$diseaseid2)
    result$pvalue_jaccard_genes <- format.pval(result$pvalue_jaccard_genes,   digits = 2)
    result$pvalue_jaccard_variants <- format.pval(result$pvalue_jaccard_variants,   digits = 2)

    codes1 <- tidyr::unnest(result[, c("diseaseid1","disease1_Vocabularies")], "disease1_Vocabularies")
    codes2 <- tidyr::unnest(result[, c("diseaseid2","disease2_Vocabularies")], "disease2_Vocabularies")
    colnames(codes2) <- colnames(codes1)
    codes <- rbind(codes1, codes2)
    found_diseases <- intersect(disease, codes$disease1_Vocabularies)
    missing_diseases <- setdiff(disease, codes$disease1_Vocabularies)
    cuis <- unique(codes[ codes$disease1_Vocabularies %in% disease, ]$diseaseid1)
    result$query <- ifelse(result$diseaseid1 %in% cuis, result$diseaseid1, result$diseaseid2)
    if( length( missing_diseases ) > 0 ) {
      diseases <- paste( paste( "   -", missing_diseases ), collapse = "\n" )
      if( warnings ) {
        warning( "\n One or more of the genes in the list is not in DISGENET plus ( '", database, "' ):\n", diseases )
      }
    }

    result <- methods::new( "DataGeNET.DGN",
                            type     = type,
                            search   = cls,
                            term       = as.character( found_diseases ),
                            database = database,
                            scoreRange =  "",
                            qresult  = result
    )

    return( result )
  } else{
    message(
      "There are no results for those parameters "
    )
  }
}
