#' Retrieves attributes associated to a disease, or list of diseases and generates an \code{DataGeNET.DGN}
#'
#' Given the name of one or multiple diseases retrieves their attributes
#' from DISGENET plus and creates an object of type \code{DataGeNET.DGN}.
#'
#' @param disease  A disease or a list of disease identifiers (CUIs, MeSH, OMIMs...)
#' @param verbose By default \code{FALSE}. Change it to \code{TRUE} to get a
#' on-time log from the function.
#' @param warnings By default \code{TRUE}. Set to \code{FALSE} to hide the warnings.
#' @param n_pags A number between 0 and 100 indicating the number of pages to retrieve from the results of the query. Default \code{100}
#' @param api_key An api key to connect to DISGENET plus. See more at https://api.disgenetplus.com/
#' @return An object of class \code{DataGeNET.DGN}
#' @examples
#' dis_atts <- disease2attribute(disease= "UMLS_C0028754" )
#' @export disease2attribute



disease2attribute <- function( disease, api_key=NULL,  n_pags = 100, verbose = FALSE, warnings = TRUE  ) {

  if(is.null(api_key)){
    api_key = Sys.getenv('DISGENETPLUS_API_KEY')
    if(api_key == ""){
      stop("This is not a valid API KEY! Please, use the function `get_disgenetplus_api_key` to get your API key from DISGENET plus")
    }
  }
  if( length( disease ) != length( unique( disease ) ) ) {
    disease <- unique( disease )
    if( warnings ) {
      warning(
        "Removing duplicates from input diseases list."
      )
    }
  }

  list_of_diseases <- paste(disease,collapse=",")

  url <- paste0( get_url_disgenetplus(), "entity/disease?disease=", list_of_diseases)

  result  <- get_data_api(url = url, n_pags = n_pags)
  if(dim(result)[1]>0){
    result <- result[ ,c("diseaseCodes" , "name", "type", "diseaseClasses_UMLS_ST", "diseaseClasses_HPO", "diseaseClasses_DO", "diseaseClasses_MSH")]
    result <- tidyr::unnest(result, cols = c("diseaseCodes" ))
    result <- as.data.frame(result)
    colnames(result) <-gsub("name", "disease_name", colnames(result))

    if(length( disease )==1){
      cls <- "single"
    }else {
      cls <- "list"
    }



    result <- methods::new( "DataGeNET.DGN",
                            type       = "disease",
                            search     = cls,
                            term       = as.character( disease ),
                            database   = "ALL",
                            scoreRange = "",
                            qresult    = result
    )
    return( result )
  } else {
    print("no results for the query")
  }
}

